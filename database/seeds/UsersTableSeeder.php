<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['name' => 'Admin', 'email' => 'admin@admin.com', 'password' => bcrypt('password'), 'is_admin' => 1, 'approved' => 1]);
    	User::create(['name' => 'User1', 'email' => 'user1@user.com', 'password' => bcrypt('password'), 'is_admin' => 0, 'approved' => 0]);
        User::create(['name' => 'User2', 'email' => 'user2@user.com', 'password' => bcrypt('password'), 'is_admin' => 0, 'approved' => 0]);
        User::create(['name' => 'User3', 'email' => 'user3@user.com', 'password' => bcrypt('password'), 'is_admin' => 0, 'approved' => 0]);
        User::create(['name' => 'User4', 'email' => 'user4@user.com', 'password' => bcrypt('password'), 'is_admin' => 0, 'approved' => 0]);
        User::create(['name' => 'User5', 'email' => 'user5@user.com', 'password' => bcrypt('password'), 'is_admin' => 0, 'approved' => 0]);

    }
}
