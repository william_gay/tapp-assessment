<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\HomePage;

class HomePageController extends Controller
{
    public function index(){
        $pages = DB::table('homepage')->select('slug', 'body')->get();
        $pageEls = array();
        foreach($pages as $page){
            $pageEls[$page->slug] = $page;
        }
        return view('index', compact('pageEls'));
    }
}
