<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pages;
use DB;

class PagesController extends Controller
{
    public function index(){
        $page = "";
        if(Auth::user()->approved){
            $page = Pages::whereSlug('approved-user')->first();
        }else{
            $page = Pages::whereSlug('unapproved-user')->first();
        }
        //dd($page);
        return view('notification', compact('page'));
    }
}
