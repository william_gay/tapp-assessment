<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;


class UserController extends Controller
{
    public function index(){
    	$users = User::all();
    	foreach($users as $user){
    		if($user->approved == 1){
    			$user->approved = true;
    		}else{
    			$user->approved = false;
    		}
    	}
      return response($users->jsonSerialize(), Response::HTTP_OK);
    }

    public function update($id){
        $user = User::findOrFail($id);
        if($user->approved == 0){
      	$user->approved = 1;
      }else{
      	$user->approved = 0;
      }
      $user->save();
      return response(null, Response::HTTP_OK);
    }
}

