require('./bootstrap');

window.Vue = require('vue');
import VueAxios from 'vue-axios';
import axios from 'axios';
import ToggleButton from 'vue-js-toggle-button';
Vue.use(VueAxios, axios);
Vue.use(ToggleButton);

Vue.component('user-list', require('./components/UserList.vue').default);

new Vue({

}).$mount('#app')