@extends('layouts.app')
@section('title', 'Home Page')
@section('content')
<header class="masthead homeBanner text-white text-center">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-xl-9 mx-auto">
              <h1 class="mb-5 fadeIn">{!! $pageEls['headline']->body !!}</h1>

            </div>
          </div>
        </div>
      </header>


<div class="container">
    <div class="row fade">
        <div class="col-md-8">
        {!! $pageEls['home-page']->body !!}
        </div>
        <div class = 'col-md-4'>
            {!! $pageEls['home-sidebar']->body !!}
        </div>
    </div>
    <div class = "row fade">
        Below is a parallax. Some people like them. Some hate them. I included it simply because I felt it would help in evaluating my css work. If you don't like it, then
        please take that into consideration rather than discounting the work because of an element on the page that you don't like.
    </div>
</div>
<div class = 'container-fluid'>
    <div class = "row fade">
        <div class = 'col-md-12'>
                <div class="parallax"></div>
                {!! $pageEls['parallax']->body !!}

        </div>
    </div>
</div>
<div class="container">
    <div class = "row">
        <div class = "col-md-12 filler">
            <div class = 'thankyou fade'>
                <h2>Thank you for taking the time to view my work and consider me for this position. As you log in and view the rest of my work, I hope you find it to your satisfaction.</h2>
            </div>
            <div class = "fade">
                <h2>The following is just filler text so as to allow the parallax to move up the page as it should.</h2>
                    <p>
                        Lorem ipsum dolor sit amet, noster intellegebat comprehensam duo id. Inani mazim inimicus ea pro. Appetere electram mediocritatem eu duo, errem liberavisse nec eu. Eu usu inani temporibus, nam tantas admodum consequuntur no, ea sit blandit facilisi.

                        Dicat appareat vis an, cum et labitur erroribus explicari. Est elit ceteros at. Eum id viderer tamquam. Mea at dolorum civibus complectitur. Pri id omnium petentium inciderint, ne nam graece mandamus. Commodo persius aliquando his eu.

                        Et cum lobortis maluisset imperdiet, magna integre ad sed. Et ferri debet omnium eam, eum erat dicam ut, his an copiosae ocurreret pertinacia. Est civibus voluptua reprimique ut, audire indoctum an mea, duo eu omnesque consequat. Nec habeo scaevola prodesset ad, no pri aeque incorrupte. Te eius repudiandae mel, aeterno lobortis prodesset no quo, timeam accusam assentior his ea.
                    </p>
            </div>
            <div class = "fade">
                <p>
                        Lorem ipsum dolor sit amet, noster intellegebat comprehensam duo id. Inani mazim inimicus ea pro. Appetere electram mediocritatem eu duo, errem liberavisse nec eu. Eu usu inani temporibus, nam tantas admodum consequuntur no, ea sit blandit facilisi.

                        Dicat appareat vis an, cum et labitur erroribus explicari. Est elit ceteros at. Eum id viderer tamquam. Mea at dolorum civibus complectitur. Pri id omnium petentium inciderint, ne nam graece mandamus. Commodo persius aliquando his eu.

                        Et cum lobortis maluisset imperdiet, magna integre ad sed. Et ferri debet omnium eam, eum erat dicam ut, his an copiosae ocurreret pertinacia. Est civibus voluptua reprimique ut, audire indoctum an mea, duo eu omnesque consequat. Nec habeo scaevola prodesset ad, no pri aeque incorrupte. Te eius repudiandae mel, aeterno lobortis prodesset no quo, timeam accusam assentior his ea.
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
