@extends('layouts.app')

@section('content')
<header class="masthead pagesBanner text-white text-center">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-xl-9 mx-auto">


            </div>
          </div>
        </div>
      </header>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Hello {{Auth::user()->name}}! You are now logged in.
                      @if(Auth::user()->is_admin)
                     <div id = "userList">

                      <user-list></user-list>
                      </div>
                      @elseif(Auth::user()->approved)
                      <h3>Your account has been approved!</h3>
                      @include('includes.approved')
                      @else
                      <h3>Your account has not been approved yet. Please check back later.</h3>
                      @endif

    </div>
    <div class ="col-md-4">
        @include('includes.sidebar')
    </div>
</div>
@endsection
