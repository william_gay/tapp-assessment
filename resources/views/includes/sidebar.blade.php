<h3>I am the included sidebar.</h3>
<p>I am available to all user home pages at this point. With some slight configuration, I can only show up on certain user pages, such as all approved users or all unapproved users
    I can also be configured to show just on admin pages or on just pages belonging to users with certain IDs.
</p>
<p>
    One of the greatest features, in my opinion, with Laravel is the simplicity with which you can use includes(partial views) to control your content.
</p>
